from flask import Flask 


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'db:3307'


@app.route('/')
def index() -> str:
    return f'<h1>Hello, Devops</h1>'
