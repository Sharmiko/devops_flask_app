## Installation

### Run application using container


1) First build the docker image</br>
`docker build -f docker/Dockerfile -t devops_flask_app .`</br>
2) Finally, run the container using `docker-compose`</br>
`docker-compose -f docker/docker-compose.yml up`</br>
   or to detach and run the container in background</br>
   `docker-compose -f docker/docker-compose.yml up -d` 
   
